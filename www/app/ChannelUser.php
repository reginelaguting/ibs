<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChannelUser extends Model
{
    protected $table = 'channel_user';
    protected $fillable = array('user_id', 'channel_id');

    public function user(){
    	return $this->belongsTo('App\User');
    }
    public function channel(){
    	return $this->belongsTo('App\Channel');
    }
}
