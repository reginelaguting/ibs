<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comment';
    protected $fillable = array('comment', 'create_at', 'user_id', 'item_id', 'updated_at');

    public function user(){
    	return $this->belongsTo('App\User');
    }
}
