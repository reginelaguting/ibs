<?php
namespace App\Http\Controllers\Auth;
use App\User;
use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
class AuthController extends Controller
{
    public function postLogin(Request $request)
    {
        $this->validate($request,[
            'username' => 'required',
            'password' => 'required',
        ]);
        if (!Auth::attempt( $request->only(['username','password']), $request->has('remember') )) {
            // Authentication passed...
            return redirect()
                ->back()
                ->with('info','Username and password does not match');
        }
        return redirect()->back();
    }
    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('home');
    }
}