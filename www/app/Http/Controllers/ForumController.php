<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

//models
use App\ChannelUser;
use App\Message;
use App\Channel;
use App\User;

//events
use App\Events\Event;
use App\Events\OnlineUser;
use App\Events\ForumEvent;

class ForumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        event(new OnlineUser($user->name));
        return view('forum.forum', ['pageTitle'=>SITE_ABRE . ' | Home', 'user'=>$user ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::user()->id;
        $message = $request->input('data.message');

        $new_message = new Message;

        $channel_user = ChannelUser::where(['user_id'=>$user_id, 'channel_id'=>1])->get()->first();

        $data = [
            'id'=>$user_id,
            'name'=>Auth::user()->name,
            'message'=>$message,
        ];

        if( !$channel_user ){
            $new_channel_user = new ChannelUser;
            $new_channel_user->user_id = $user_id;
            $new_channel_user->channel_id = 1;
            $new_channel_user->save();

            $new_message->body = $message;
            $new_message->channel_user_id = $new_channel_user->id;
            $new_message->save();

            $data = [
                'id'=>$user_id,
                'name'=>Auth::user()->name,
                'message'=>$message,
            ];

            event(new ForumEvent($data));

            return response()->json(['status'=>'OK', 'message'=>'Successfully store message', 'data'=> json_encode($data) ]);
        }

        $new_message->body = $message;
        $new_message->channel_user_id = $channel_user->id;
        $new_message->save();

        event(new ForumEvent($data));

        return response()->json(['status'=>'OK', 'message'=>'Successfully store message', 'data'=> json_encode($data) ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $channel = Channel::find($id);
        $message_data = array();

        foreach($channel->message as $cm){
            $data = [
                'user'=>$cm->channel_user->user->name,
                'message'=>$cm->body,
                'created_at'=>$cm['created_at']->toDateTimeString(),
            ];
            array_push( $message_data, $data );
        }

        $counter;

        for($i=0; $i<count($message_data); $i++){

            if( $i==count($message_data)-1 ){
                break;
            }

            $counter = $message_data[$i];
            if( $message_data[$i+1]['created_at'] < $counter['created_at'] ){
                $message_data[$i] = $message_data[$i+1];
                $message_data[$i+1] = $counter;
            }
        }


        return response()->json(['status'=>'OK', 'message'=>'Successfully get channel user', 'data'=> $message_data ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
