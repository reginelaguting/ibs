<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Item;
use App\User;
use App\Type;
use App\Category;
use Crypt;
use App\Http\Controllers\Controller;
use Auth;
use DateTime;
use Illuminate\Support\Facades\Redirect;
use Hash;
use App\ImageUpload;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::check()){
            $user = Auth::user();
            // $pass = Hash::make('123');
            $for_sell = Item::where('type_id', 2)->orderBy('updated_at', 'desc')->paginate(15);
            $looking_for = Item::where('type_id', 1)->orderBy('updated_at', 'desc')->paginate(15);

            $images = ImageUpload::get();

            return view('index', ['for_sell'=>$for_sell, 'looking_for'=>$looking_for, 'user'=>$user, 'images'=>$images])
                ->with('pageTitle', SITE_ABRE . ' | Home')
                ->with('user', $user);
        }

        return view('user.create')
            ->with('pageTitle', SITE_ABRE . ' | Register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Sometimes you may wish to stop running validation rules on an attribute after the first validation failure. To do so, assign the bail rule to the attribute: 'title' => 'bail|required|unique:posts|max:255'
        $this->validate($request, [
            'username' => 'required|unique:user|max:255',
            'email' => 'required|unique:user|max:255',
            'password' => 'required',
        ]);

        $now = new DateTime();
        $timestamp = $now->getTimestamp();
        $user = new User;

        $user->username = $request->input('username');
        $user->password = Hash::make($request->input('password') );
        $user->facebook_id = $request->input('facebook_id');
        $user->name = $request->input('name');
        $user->email = $request->input('fb_email');
        $user->remember_token = $request->input('token');
        $user->role_id = 2;
        $user->city_id = 792;
        $user->created_at = $timestamp;
        $user->save();
        return Redirect::back()->withSuccess('Successfully registered ' . $user->name);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id', $id)->get()->first();
        $items = Item::where('user_id', $id)->get();

        if($user){
            return view('user.show')
                ->with('user', $user)
                ->with('items', $items)
                ->with('pageTitle', SITE_ABRE . ' | ' . $user->name);
        }

        return 'ERROR 404';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', Auth::user()->id)
                    ->get()
                    ->first();

        return view('user.edit')
                ->with('pageTitle', SITE_ABRE . ' | Edit Profile')
                ->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        // Sometimes you may wish to stop running validation rules on an attribute after the first validation failure. To do so, assign the bail rule to the attribute: 'title' => 'bail|required|unique:posts|max:255'
        $this->validate($request, [
            //unique:user,id,:id means that exclude this user from checking if the username or email exist
            'username' => 'required|unique:user,id,:id|max:255',
            'email' => 'required|unique:user,id,:id|max:255',
            'name' => 'required|max:50',
            'password_confirm' => 'same:password',
        ]);

        $user = User::where('id', Auth::user()->id )
                    ->get()
                    ->first();

        $now = new DateTime();
        $timestamp = $now->getTimestamp();


        $user->name = $request->input('name');
        $user->username = $request->input('username');
        $user->email = $request->input('email');
        $user->contact = $request->input('contact');
        $user->updated_at = $timestamp;
        $user->address = $request->input('address');
        //has password
        $pass = Hash::make($request->input('password'));

        if(!empty( $request->input('password') )){
            $user->password = $pass;
        }

        $user->role_id = 2;
        $user->city_id = 792;

        $user->save();

        return redirect()->action('UserController@show', $user->id)
                        ->with('update', 'Successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
