<?php

namespace App\Listeners;

use App\Events\ForumEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForumListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ForumEvent  $event
     * @return void
     */
    public function index(ForumEvent $event)
    {
        //
    }
}
