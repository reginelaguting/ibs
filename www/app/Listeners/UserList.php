<?php

namespace App\Listeners;

use App\Events\OnlineUser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserList
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OnlineUser  $event
     * @return void
     */
    public function index(OnlineUser $event)
    {
        //
    }
}
