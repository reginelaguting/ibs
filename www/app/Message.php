<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'message';
    protected $fillable = array('channel_user_id', 'body');

    public function channel_user(){
    	return $this->belongsTo('App\ChannelUser');
    }
}
