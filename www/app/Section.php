<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $table = 'section';

    public function item(){
    	return $this->hasManyThrough('App\Item', 'App\Category');
    }
}
