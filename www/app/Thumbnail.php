<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thumbnail extends Model
{
    protected $table = 'thumbnail';
    protected $fillable = array('name', 'item_id', 'created_at', 'updated_at');

    public function item(){
    	return $this->belongsTo('App\Item');
    }
}
