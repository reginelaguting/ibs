@extends('layout.lte-default')

@section('content')
	
		
		<div class="row">
			
			<div class="col-md-8">
				
				<strong>IBS | Iligan Buy and Sell</strong>
				<br>
				<br>
				<p>The <strong>{{ SITE_NAME }}</strong> The Iligan Buy and Sell (IBS) is an online community where private individuals can engage in selling and buying goods and services in an ultra-convenient hassle-free style. In IBS, we take the marketing level of young and starting entrepreneurs to an even higher and wider platform using the World Wide Web. The IBS is mainly based in Iligan City.
				<br>
				<br>
				But though locally based the infinite goal and well-established vision of its founder, its co-creators and with your help, together we will continuously look and move towards greater heights. For we are committed to bringing a better landscape of market development by enabling our clients to more choices and better preferences as they buy and sell in the information superhighway.</p>



			</div>


		</div>

		


@endsection