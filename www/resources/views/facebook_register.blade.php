<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Iligan Buy and Sell | The best place to Buy a house, Sell a car or Find a job in Iligan City.
</title>

    <!-- Bootstrap 3.3.6 -->
    <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />

    <!-- Custom Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="{{ asset('/css/animate.min.css') }}" type="text/css">

    <!-- VEGAS CSS -->
    <link rel="stylesheet" href="{{ asset('/js/vegas/vegas.min.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('/css/creative.css') }}" type="text/css">

    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top"><img alt="Brand" width="20" height="20" src="{{ asset('/img/logo.png') }}" style="display: inline;"> Iligan Buy and Sell</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="#about">About</a>
                    </li>
                    <!-- <li>
                        <a class="page-scroll" href="#items">Categories</a>
                    </li> -->
                    <li>
                        <a class="page-scroll" href="#contact">Contact</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#login">Login</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header>
        <div class="header-content">
            <div class="header-content-inner">

                <div class="row">

                    <div class="col-md-5">

                        <h1>Iligan Buy and Sell</h1>
                        <!-- <hr> -->
                        <p>The best place to Buy a house, Sell a car or Find a job in Iligan City.</p>
                        <!-- <a href="#about" class="btn btn-primary btn-xl page-scroll">Login</a> -->




                    </div>

                    <div id="login" class="col-md-3 col-md-offset-4">

                        <div class="login-wrapper">

                            <h4>Please Login</h4>
                            @if( Session::has( 'info' ))
                            <div class="alert alert-danger">
                                 {{ Session::get( 'info' ) }} <!-- here to 'withWarning()' -->
                            </div>
                            @elseif(session('error_message'))
    				     		<div class="alert alert-danger">
    				     			{{ session('error_message') }}
    				     		</div>
                            @endif
                            <form role="form" method="post" action="{{ route('auth.login') }}">

                                <input type="hidden" name="_token" value="{{ Session::token() }}">

                                <div class="form-group">
                                    <!-- <label for="username">Username:</label> -->
                                    <input type="text" class="form-control input-sm" id="username" placeholder="Username" name="username">
                                </div>

                                <div class="form-group">
                                    <!-- <label for="password">Password:</label> -->
                                    <input type="password" class="form-control input-sm" id="password" placeholder="Password" name="password">
                                </div>

                                <button type="submit" class="btn btn-primary">Login</button>
                                <a class="btn btn-success" href="{{ route('user.create') }}">Register</a>
                                <br>OR<br>
                                <a class="btn btn-primary" href="/redirect"><i class="fa fa-facebook"></i>b Login</a>

                            </form>

                        </div>

                    </div>

                </div>



            </div>
        </div>
    </header>

	<div id="myModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Iligan Buy and Sell| Come on Join Us!</h4>
	      </div>
	      <div class="modal-body">
			  {{-- Profile Section --}}
			  <div class="col-md-12">

				  <div class="panel panel-success">
					  <div class="panel-heading">
						  <h3 class="panel-title">Facebook Profile</h3>
					  </div>
					  <div class="panel-body">
						  <div class="row">
							  <div class="col-md-6">
								  <h2>{{ $fb_name }}</h2>
								  <p>{{ $fb_email }}</p>
							  </div>
							  <div class="col-md-6"><img src="{{ $fb_avatar }}"></div>
						  </div>

						  <div class="row">

							  <!-- form start -->
							  <div class="col-md-12">
								  <form method="POST" action="/fblogin">

									  @if($errors)
										  @foreach($errors->all() as $error)
											  <div class="alert alert-danger">
												  {{ $error }}
											  </div>
										  @endforeach
									  @endif

									  {!! csrf_field() !!}
									  <input type="hidden" name="facebook_id" value="{{ $fb_id }}"></input>
									  <input type="hidden" name="name" value="{{ $fb_name }}"></input>
									  <input type="hidden" name="email" value="{{ $fb_email }}"></input>
									  <input type="hidden" name="token" value="{{ $fb_token }}"></input>
									  <input type="hidden" name="username" value="{{ $fb_email }}">

									  <div class="form-group">
										  <button type="submit" class="btn btn-primary">Create my Account Now!</button>
									  </div>

								  </form>
							  </div>
							  <!--  form end -->

						  </div>

					  </div>
				  </div>



			  </div>
			  {{-- //Profile Section --}}
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>

	  </div>
	</div>

    <section class="bg-primary" id="about">
        <div class="container">
            <div class="row">

                <div class="col-md-8">

                    <strong>IBS | Iligan Buy and Sell</strong>
                    <br>
                    <br>
                    <p>The <strong>{{ SITE_NAME }}</strong> The Iligan Buy and Sell (IBS) is an online community where private individuals can engage in selling and buying goods and services in an ultra-convenient hassle-free style. In IBS, we take the marketing level of young and starting entrepreneurs to an even higher and wider platform using the World Wide Web. The IBS is mainly based in Iligan City.
                    <br>
                    <br>
                    But though locally based the infinite goal and well-established vision of its founder, its co-creators and with your help, together we will continuously look and move towards greater heights. For we are committed to bringing a better landscape of market development by enabling our clients to more choices and better preferences as they buy and sell in the information superhighway.</p>



                </div>


            </div>
        </div>
    </section>


    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Advertise with us!</h2>
                    <hr class="primary">
                    <p>Ready to boost your sales with us? That's great! Give us a call or send us an email and we will get back to you as soon as possible!</p>
                </div>
                <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x wow bounceIn"></i>
                    <p>+63 228-0443</p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x wow bounceIn" data-wow-delay=".1s"></i>
                    <p><a href="mailto:admin@ibsph.com">ryanbalisi378@gmail.com</a></p>
                </div>
            </div>
        </div>
    </section>

    <!-- jQuery 2.1.3 -->
    <script src="{{ asset ("/bower_components/admin-lte/plugins/jQuery/jQuery-2.2.0.min.js") }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>

    <!-- Plugin JavaScript -->
    <script src="{{ asset('/js/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.fittext.js') }}"></script>
    <script src="{{ asset('/js/wow.min.js') }}"></script>
    <script src="{{ asset('/js/vegas/vegas.min.js') }}"></script>
    <script type="text/javascript">

        $("header").vegas({
            slides: [
                { src: "/img/bg/1.jpg" },
                { src: "/img/bg/2.jpg" },
                { src: "/img/bg/3.jpg" },
                { src: "/img/bg/4.jpg" },
                { src: "/img/bg/5.jpg" },
            ],
            overlay: '/js/vegas/overlays/09.png'
        });

    </script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('/js/creative.js') }}"></script>

    <style type="text/css">

        .navbar {
            border: none;
        }

    </style>

	<script type="text/javascript">
	$(document).ready(function() {
		$('#myModal').modal('show');
	});
	</script>

</body>

</html>
