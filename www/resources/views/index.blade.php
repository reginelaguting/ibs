@extends('layout.lte-default')

@section('content')


		<div class="row">

			<div class="col-md-12">

				<div class="row">

					<div class="col-md-12">

						@if( Session::has( 'success' ))
							<div class="alert alert-success">
								{{ Session::get( 'success' ) }}
							</div>

						@elseif(session('error_message'))
				     		<div class="alert alert-danger">
				     			{{ session('error_message') }}
				     		</div>
						@endif


					</div>

				</div>

				<div class="row">

					{{-- Selling Section / Categories Section --}}
		  			<div class="col-md-6">

		  				<div class="panel panel-success">
							<div class="panel-heading">
							    <h3 class="panel-title">Sellers Section</h3>
							</div>
							<div id="selling-section" class="panel-body ibs-panel-body">

								@foreach($for_sell as $fs)

									<div class="media">

										<div class="media-left">
											<a href="{{ route('item.show', $fs->id) }}">
												@foreach($thumbnails as $thumbnail)
													@if($thumbnail->item_id == $fs->id)
														<img class="media-object" data-src="{{ asset('/www/public/img/'.$thumbnail->name) }}" alt="Description Here" src="{{ asset('/www/public/img/'.$thumbnail->name) }}" width="64" height="64">
													@endif()
												@endforeach
											</a>
										</div>

										<div class = "media-body">
											<h4 class = "media-heading"><a href="{{ route('item.show', $fs->id) }}">{{$fs->name}}</a></h4>
											<p id="item-description">{{ $fs->description }}</p>
											<small>
												<em>
													<span class="price">₱ {{ $fs->price }}</span>
													Posted by: <span class="posted_by">{{ $fs->user->name }}</span>
													Created at: <span class="created_at">{{ date_format($fs->created_at,"F j, Y g:i a") }}</span>
												</em>
											</small>
										</div>

									</div>

								@endforeach

								<div class="pagination"> {{ $for_sell->links() }} </div>

							</div>
						</div>



		  			</div>
		  			{{-- //Selling Section / Categories Section--}}

		  			{{-- Buying Section --}}
		  			<div class="col-md-6">

		  				<div class="panel panel-info">
							<div class="panel-heading">
							    <h3 class="panel-title">Buyers Section</h3>
							</div>
							<div id="buying-section" class="panel-body ibs-panel-body">
							    @foreach($looking_for as $lf)

									<div class="media">

										<div class="media-left">
											<a href="{{ route('item.show', $lf->id) }}">
												@foreach($thumbnails as $thumbnail)
													@if($thumbnail->item_id == $lf->id)
														<img class="media-object" data-src="{{ asset('/www/public/img/'.$thumbnail->name) }}" alt="Description Here" src="{{ asset('/www/public/img/'.$thumbnail->name) }}" width="64" height="64">
													@endif()
												@endforeach
											</a>
										</div>

										<div class = "media-body">
											<h4 class = "media-heading"><a href="{{ route('item.show', $lf->id) }}">{{$lf->name}}</a></h4>
											<p style="text-overflow: ellipsis;">{{ $lf->description }}</p>
											<small>
												<em>
													<span class="price">₱ {{ $lf->price }}</span>
													Posted by: <span class="posted_by">{{ $lf->user->name }}</span>
													Created at: <span class="created_at">{{ date_format($lf->created_at,"F j, Y g:i a") }}</span>
												</em>
											</small>
										</div>

									</div>

								@endforeach

								<div class="pagination"> {{ $looking_for->links() }} </div>
							</div>
						</div>



		  			</div>
		  			{{-- //Buying Section --}}

				</div>

			</div>


		</div>


@endsection
