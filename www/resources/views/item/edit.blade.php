@extends('layout.lte-default')

@section('content')

	<div class="container-fluid">

		<div id="search-from-item-create" style="display:none" class="row">

			<div class="col-md-12">

				<div class="row">

					{{-- Selling Section / Categories Section --}}
					<div class="col-md-6">

						<div class="panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title">Selling Section</h3>
							</div>
							<div id="selling-section" class="panel-body ibs-panel-body">

							</div>
						</div>



					</div>
					{{-- //Selling Section / Categories Section--}}

					{{-- Buying Section --}}
					<div class="col-md-6">

						<div class="panel panel-info">
							<div class="panel-heading">
								<h3 class="panel-title">Buying Section</h3>
							</div>
							<div id="buying-section" class="panel-body ibs-panel-body">

							</div>
						</div>



					</div>
					{{-- //Buying Section --}}

				</div>

			</div>


		</div>

		<div id="item-edit" class="row">

			<div class="col-md-10">

				<div class="row">

					{{-- Selling Section / Categories Section --}}
		  			<div class="col-md-12">

		  				<div class="panel panel-default">
							<div class="panel-heading">
							    <h3 class="panel-title">Item Information</h3>
							</div>
							<div class="panel-body">

							    <div class="col-md-6">


									@if($errors)
										@foreach($errors->all() as $error)
											<div class="alert alert-danger">
												{{ $error }} | Please choose section first.
											</div>
										@endforeach
									@endif

									<form method="POST" action="{{route('item.update', $item->id)}}" enctype="multipart/form-data">

								    	{!! csrf_field() !!}
										{!! method_field('put') !!}

								    	<div class="form-group">
										    <label for="inputSection">Type</label>
										   	<select name="type_id" class="form-control input-sm">
										   		@foreach($types as $type)
										   			<option value="{{$type->id}}">{{ $type->name }}</option>
										   		@endforeach
											</select>
										</div>

										<div class="form-group">
											<label for="inputItemName">Item Name</label>
	    									<input type="text" class="form-control" name="name" id="inputItemName" value="{{ $item->name }}">
	    								</div>

	    								<div class="form-group">
											<label for="inputItemDescription">Item Description</label>
	    									<textarea name="description" rows="4" cols="50">{{ $item->description }}</textarea>
	    								</div>

										<div class="form-group">
										    <label for="inputCategory">Section</label>
										   	<select onChange="show_categories();" id="section_id" name="section_id" class="form-control input-sm">
										   		<option value="0">Please select property section</option>
										   		@foreach($sections as $section)
										   			<option value="{{ $section->id }}">{{ $section->name }}</option>
										   		@endforeach
											</select>
										</div>

										<div id="sectionloading" class="form-group" style="display:none">
	                                        <center><img src="/img/sectionloading.gif" alt="" /></center>

	                                    </div>

										<div id="category_view" style="display:none" class="form-group">
										    <label>Category</label>
										   	<select id="category_id" name="category_id" class="form-control input-sm">

											</select>
										</div>

										<div class="form-group">
											<label for="inputPrice">Price or Budget (Php)</label>
	    									<input name="price" type="number" class="form-control" id="inputPrice" value="{{ $item->price }}">
	    								</div>

										@if($images==null)
											<div class="form-group">
												<label >Update Image</label>
												<input id="sample_input" name="images" type="file" class="form-control">
											</div>
										@endif
										<div class="form-group">
	    									<button type="submit" class="btn btn-primary">Update</button>
	    								</div>
							    	</form>
								</div>

								<div class="col-md-6">
								@if($images!=null)
									<img width="304" height="236" src="{{ asset('/www/public/img/'.$images->name) }}"><br>
									<a href="{{ route('item.destroy', $item->id) }}"><button class="btn btn-warning">Delete this item</button></a>
								@endif
								</div>

							</div>
						</div>


		  			</div>
		  			{{-- //Selling Section / Categories Section--}}


				</div>

			</div>

			<div class="col-md-2">

					@if(!Auth::check())

					@include('layout.left-side')

					@else
					@include('layout.left-side')

					@endif


			</div>

		</div>

	</div>

@endsection
