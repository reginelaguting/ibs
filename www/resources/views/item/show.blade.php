@extends('layout.lte-default')

@section('content')

	<div class="container-fluid">

		<div class="row">

			<div class="col-md-10">

				<div class="row">

					{{-- Selling Section / Categories Section --}}
		  			<div class="col-md-12">

		  				<div class="panel panel-default">
							<div class="panel-heading">
							    <h3 class="panel-title">Item Information</h3>
							</div>
							<div class="panel-body">

							    <div class="col-md-6">
									<h1>{{$item->name}}</h1>
									<p>Price: ₱ {{ $item->price }}</p>
									<p>Posted by: <a href="{{ route('user.show', $item->user->id) }}">{{$item->user->name}}</a> | {{ $item->created_at }}</p><br>
									<p>
										<h3>Description</h3>
										{{ $item->description }}
									</p>
								</div>

								<div class="col-md-6">
								@if($images!=null)
									<img width="304" height="236" src="{{ asset('/www/public/img/'.$images->name) }}">
								@endif
								</div>

							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading">
							    <h3 class="panel-title">Comment(s)</h3>
							</div>
							<div class="panel-body">
							    @if(Auth::check())
									<form method="POST" action="{{ route('comment.item.user.store', [$item->id, Auth::user()->id]) }}">

								    	{!! csrf_field() !!}

										<div class="form-group">
	    									<input name="comment" type="text" class="form-control" placeholder="write comment...">
	    									<button type="submit" class="btn btn-primary">Post</button>
	    								</div>

								    </form>
								@else <p>Please Log in to post comment(s)</p>
							    @endif

							    @foreach($comments as $comment)
							    	<p>{{$comment->user->name}} says: {{$comment->comment}}</p>
							    @endforeach

							</div>
						</div>



		  			</div>
		  			{{-- //Selling Section / Categories Section--}}


				</div>

			</div>

			<div class="col-md-2">

					@if(!Auth::check())

					@include('layout.left-side')

					@else
					@include('layout.left-side')

					@endif


			</div>

		</div>

	</div>

@endsection
