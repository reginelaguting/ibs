<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{{ $pageTitle }}</title>
	<!-- Tell the browser to be responsive to screen width -->
  	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<!-- Bootstrap 3.3.6 -->
	<link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />

	<!-- Font Awesome -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

	<link href="{{ asset('/components/imgareaselect/css/imgareaselect-default.css') }}" rel="stylesheet" media="screen">

	<!-- Ionicons -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

  	<!-- Theme style -->
    <link href="{{ asset("/bower_components/admin-lte/dist/css/AdminLTE.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
              page. However, you can choose any other skin. Make sure you
              apply the skin class to the body tag so the changes take effect.
        -->

    <link href="{{ asset("/bower_components/admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />


	<link rel="stylesheet" href="{{ asset('/css/jquery.awesome-cropper.css') }}">


	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

	<link href="{{ asset('/css/style.css') }}" rel="stylesheet">

</head>
<body>
<div class="wrapper">

	<!-- Main Header -->
	<header class="main-header">

		<!-- Logo -->
    	<a href="{{ url('/') }}" class="logo">
	      	<!-- mini logo for sidebar mini 50x50 pixels -->
	      	<span class="logo-mini"><img alt="Brand" width="20" height="20" src="{{ asset('/img/logo.png') }}"></span>
	      	<!-- logo for regular state and mobile devices -->
	      	<span class="logo-lg">{{ SITE_NAME }}</span>
    	</a>

		<nav class="navbar navbar-static-top">

			<div class="container-fluid">

				<!-- Sidebar toggle button-->
			    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			    	<span class="sr-only">Toggle navigation</span>
			    </a>

				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="active"><a href="{{ url('/') }}">Home</a></li>
						<li><a href="{{ url('/') }}">Forums</a></li>
						<li><a href="{{ url('/about') }}">About Us</a></li>
					</ul>

					<form class="navbar-form navbar-left" role="search" method="GET" action="{{ url('/search') }}">
				        <div class="form-group">
				          <input name="query" type="text" class="form-control" placeholder="Search an item">
				        </div>
				        <button type="submit" class="btn btn-default btn-xs">Search</button>
				        <a class="btn btn-warning btn-xs" href="{{ url('/item/create') }}"><i class="fa fa-plus-circle"></i> Post an item for free!</a>
				    </form>



					{{-- <ul class="nav navbar-nav navbar-right">
						@if (Auth::guest())
							<li><a href="{{ url('/auth/login') }}">Login</a></li>
							<li><a href="{{ route('item.create') }}">Register</a></li>
						@elseif(Auth::check())
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="{{ route('logout') }}">Logout</a></li>
								</ul>
							</li>
						@endif
					</ul> --}}

					@if(Auth::check())
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
								<span class="glyphicon glyphicon-briefcase"></span>
								Inventory
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/myitem', Auth::user()->id) }}">My Item(s)</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
								<span class="glyphicon glyphicon-user"></span>
								{{ Auth::user()->name }}
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ route('user.show', Auth::user()->id) }}">View Profile</a></li>
								<li><a href="{{ route('user.edit', Auth::user()->id) }}">Update Profile</a></li>
								<li><a href="{{ route('logout') }}">Logout</a></li>
							</ul>
						</li>
					</ul>
					@endif()


				</div>
			</div>
		</nav>

	</header>

	@yield('content')

</div>
	<!-- Scripts -->
	<script src="{{ asset('/js/jquery.min.js') }}"></script>
	<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('/js/main.js') }}"></script>
	<script src="{{ asset('/components/imgareaselect/scripts/jquery.imgareaselect.js') }}"></script>
	<script src="{{ asset('/build/jquery.awesome-cropper.js') }}"></script>
</body>
</html>
