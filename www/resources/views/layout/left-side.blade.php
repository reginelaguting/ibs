				@if(!Auth::check())
				<a href="/redirect" class="btn btn-primary btn-sm btn-block"><i class="fa fa-facebook"></i> Facebook Login</a>

				<div class="panel panel-default">
					<div class="panel-heading">
					    <h3 class="panel-title">User Login</h3>
					</div>
					<div class="panel-body">

				    	@if (session('info'))
                            <div class="alert alert-danger">{{ session('info') }}</div>
                        @endif

					    @if($errors)
					     	@foreach($errors->all() as $error)
					     		<div class="alert alert-danger">
					     			{{ $error }}
					     		</div>
					     	@endforeach
						@endif

						<form role="form" method="post" action="{{ route('auth.login') }}">

							<input type="hidden" name="_token" value="{{ Session::token() }}">

							<div class="form-group">
							    <label for="username">Username:</label>
							    <input type="text" class="form-control input-sm" id="username" placeholder="Username" name="username">
							</div>

							<div class="form-group">
							    <label for="password">Password:</label>
							    <input type="password" class="form-control input-sm" id="password" placeholder="Password" name="password">
							</div>

							<button type="submit" class="btn btn-primary btn-xs">Login</button>
							<a class="btn btn-success btn-xs" href="{{ route('user.create') }}">Register</a>

						</form>

					</div>
				</div>
				@endif

				<div class="panel panel-default">
					<div class="panel-heading">
					    <h3 class="panel-title">Sections</h3>
					</div>
					<div class="panel-body">

					    <ul id="section-list">

					    </ul>

					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
					    <h3 class="panel-title">Information</h3>
					</div>
					<div class="panel-body">



					</div>
				</div>
