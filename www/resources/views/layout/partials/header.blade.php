    <!-- Main Header -->
	<header class="main-header">

		<!-- Logo -->
    	<a href="{{ url('/') }}" class="logo">
	      	<!-- mini logo for sidebar mini 50x50 pixels -->
	      	<span class="logo-mini"><img alt="Brand" width="20" height="20" src="{{ asset('/img/logo.png') }}"></span>
	      	<!-- logo for regular state and mobile devices -->
	      	<span class="logo-lg"><img alt="Brand" width="20" height="20" src="{{ asset('/img/logo.png') }}"> {{ SITE_NAME }}</span>
    	</a>

    	<!-- Header Navbar -->
		<nav class="navbar navbar-static-top" role="navigation">



				<!-- Sidebar toggle button-->
			    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			    	<span class="sr-only">Toggle navigation</span>
			    </a>

			    <!-- Navbar Right Menu -->
				<div class="navbar-custom-menu">

					<ul class="nav navbar-nav">


						<!-- /.messages-menu -->

	                    <!-- Notifications Menu
	                    <li class="dropdown notifications-menu">
	                        Menu toggle button
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	                            <i class="fa fa-bell-o"></i>
	                            <span class="label label-warning">10</span>
	                        </a>
	                        <ul class="dropdown-menu">
	                            <li class="header">You have 10 notifications</li>
	                            <li>
	                                Inner Menu: contains the notifications
	                                <ul class="menu">
	                                    <li>
											start notification
	                                        <a href="#">
	                                            <i class="fa fa-users text-aqua"></i> 5 new members joined today
	                                        </a>
	                                    </li>
										end notification
	                                </ul>
	                            </li>
	                            <li class="footer"><a href="#">View all</a></li>
	                        </ul>
	                    </li> -->

                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <!-- <img src="{{ asset("/bower_components/admin-lte/dist/img/user2-160x160.jpg") }}" class="user-image" alt="User Image"/> -->
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs">{{Auth::user()->name}}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <!-- <img src="{{ asset("/bower_components/admin-lte/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image" /> -->
                                    <p>
                                        {{Auth::user()->email}} IBS
                                        <small>Member since {{Auth::user()->created_at}}</small>
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <li class="user-body">
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Followers</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Sales</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Friends</a>
                                    </div>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="{{route('user.show', Auth::user()->id)}}" class="btn btn-default btn-flat">Profile</a>
										<a href="{{ route('user.edit', Auth::user()->id) }}" class="btn btn-default btn-flat">Update</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ route('logout') }}" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>

					</ul>

				</div>


		</nav>

	</header>
