@extends('layout.lte-default')

@section('content')

	<div class="container-fluid">


		<div id="search-from-item-create" style="display:none" class="row">

			<div class="col-md-12">

				<div class="row">

					{{-- Selling Section / Categories Section --}}
		  			<div class="col-md-6">

		  				<div class="panel panel-success">
							<div class="panel-heading">
							    <h3 class="panel-title">Selling Section</h3>
							</div>
							<div id="selling-section" class="panel-body ibs-panel-body">

							</div>
						</div>



		  			</div>
		  			{{-- //Selling Section / Categories Section--}}

		  			{{-- Buying Section --}}
		  			<div class="col-md-6">

		  				<div class="panel panel-info">
							<div class="panel-heading">
							    <h3 class="panel-title">Buying Section</h3>
							</div>
							<div id="buying-section" class="panel-body ibs-panel-body">

							</div>
						</div>



		  			</div>
		  			{{-- //Buying Section --}}

				</div>

			</div>


		</div>

		<div id="user-show" class="row">

			<div class="col-md-10">

				<div class="row">

					@if( session( 'update' ))
					    <div class="alert alert-success">
							{{ session( 'update' ) }}
						</div>
					@endif

					{{-- Selling Section / Categories Section --}}
		  			<div class="col-md-12">

		  				<div class="panel panel-default">
							<div class="panel-heading">
							    <h1>{{ $user->name }}</h1>
							    <h4>{{ $user->email }}</h4>

							</div>

							<div class="panel-body">

								<p>Account type:
								@if($user->role_id==2)
									Regular
								@else
									Premium
								@endif
								</p>

								<p>Contact: {{ $user->contact }}</p>
								<p>Address: {{ $user->address }}</p>

							</div>

						</div>



		  			</div>
		  			{{-- //Selling Section / Categories Section--}}


				</div>


				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3>{{ $user->name }} posted this item(s)</h3>
							</div>

							<div class="panel-body">
								{{-- Selling Section / Categories Section --}}
					  			<div class="col-md-6">

					  				<div class="panel panel-success">
										<div class="panel-heading">
										    <h3 class="panel-title">For Sell</h3>
										</div>
										<div class="panel-body">
											@foreach($items as $item)
												@if ($item->type_id == 2)
													<h2><a href="{{ route('item.show', $item->id) }}">{{$item->name}}</a></h2>
													<p>{{ $item->description }}</p>
												@endif
											@endforeach
										</div>
									</div>
					  			</div>
					  			{{-- //Selling Section / Categories Section--}}

					  			{{-- Buying Section --}}
					  			<div class="col-md-6">

					  				<div class="panel panel-info">
										<div class="panel-heading">
										    <h3 class="panel-title">Looking For This Item(s)</h3>
										</div>
										<div class="panel-body">
										    @foreach($items as $item)
												@if ($item->type_id == 1)
													<h2>
														<a href="{{ route('item.show', $item->id) }}">{{$item->name}}</a>
													</h2>
													<p>{{ $item->description }}</p>
												@endif
											@endforeach
										</div>
									</div>
					  			</div>
					  			{{-- //Buying Section --}}
				  			</div>
				  		</div>
			  		</div>
				</div>


			</div>

			<div class="col-md-2">

					@include('layout.left-side')

			</div>

		</div>

	</div>

@endsection
